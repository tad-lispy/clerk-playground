(ns cli
  (:require [nextjournal.clerk :as clerk])
  (:require [clojure.java.io :as io]))

(defn regular-file? [file]
  (.isFile file))

(defn file-name [file]
  (.getName file))

(defn publish [opts]
  (let [paths (->> "notebooks"
                   (io/file)
                   (file-seq)
                   (filter regular-file?)
                   (map file-name)
                   (mapv #(str "notebooks/" %)))]

    (clerk/build-static-app! {:out-path "build/"
                              :paths paths})))

(defn develop [opts]
  (clerk/serve! {:watch-paths ["notebooks" "src"]
               :show-filter-fn #(clojure.string/starts-with? % "notebooks")}))
