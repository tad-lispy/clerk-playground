include Makefile.d/defaults.mk

all: ## Run unit tests and build the program (DEFAULT)
all: build
.PHONY: all

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@echo "Useage: make [ goals ]"
	@echo
	@echo "Available goals:"
	@echo
	@cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

build: ## Produce HTML files from notebooks
	clj -X cli/publish
.PHONY: build

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: build
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

### DEVELOPMENT

repl: ## Start Clojure repl
repl:
	clojure -M:rebel
.PHONY: repl

develop: ## Start development server for notebooks
develop:
	clj -X cli/develop
.PHONY: develop

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

test-timelog-query: ## Send query to GitLab and get the timelog. Requires <token> variable.
test-timelog-query:
	test $(token)
	http POST "https://gitlab.com/api/graphql" \
		"query=@timelog.graphql" \
		"Authorization: Bearer $(token)" \
	| jq --from-file timelog.jq
.PHONY: test-timelog-query

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
