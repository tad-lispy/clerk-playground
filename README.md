Clerk Playground
================

An experiment in using [Clerk][] for client dashboards.


## Goals:

  - Can I use clerk for Software Garden clients' dashboards?


## Tasks

  - [x] Setup a development environment
  - [ ] Make API request to GitLab
  
  
## Questions / issues

  1. When exporting static site how to make one notebook a home page?

  2. How can I link from one notebook to another?
  
     I can use `#/notebooks/xyz.clj`. It works in exported HTML files, but not in development.
  
   4. Links are not decorated (CSS)
   
   5. Static pages are not really static
   
      There is a lot of JS loaded from 
      
      - `https://storage.googleapis.com/nextjournal-cas-eu/d`
      - `https://cdn.jsdelivr.net/npm/`
      
      I wish Klerk would just write HTML that could be rendered without JS.

[Clerk]: https://github.com/nextjournal/clerk
