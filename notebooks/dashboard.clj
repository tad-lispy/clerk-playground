;; # Dashboard
;;
;; A link to [another notebook](#/notebooks/second.clj).

^{:nextjournal.clerk/visibility :hide}
(ns dashboard
  (:require [nextjournal.clerk :as clerk]))

(map range (range 3))

;; ## Getting data from a file

(slurp "./data.csv")

(defn whatever [options]
  (println (+ 2 5)))
