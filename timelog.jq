.data
.group
.timelogs
.nodes
[]
|
{
  worker: .user.name,
  issue: .issue.title,
  url: .issue.webUrl,
  date: .spentAt
}
