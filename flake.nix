{
  description = "Tad Lispy's Clerk Playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.gnumake
            pkgs.clojure
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "clerk-playground";
          src = self;
          buildInputs = shared-inputs ++ [];
        };

        devShell = pkgs.mkShell {
          name = "clerk-playground-dev-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
            pkgs.jq
            pkgs.httpie
          ];
        };
      }
    );
}
